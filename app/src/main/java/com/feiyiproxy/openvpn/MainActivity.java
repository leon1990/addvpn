package com.feiyiproxy.openvpn;

import android.content.ComponentName;
import android.content.Intent;
import android.preference.PreferenceActivity;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "openvpn";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        //TODO 开启VPN添加界面
        Intent vpnIntent = new Intent();
        vpnIntent.setAction("android.net.vpn.SETTINGS");
        this.startActivity(vpnIntent);

        //com.android.settings/com.android.settings.Settings$VpnSettingsActivity
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        ComponentName componentName = new ComponentName("com.android.settings",
//                "com.android.settings.Settings$VpnSettingsActivity");
//        intent.setComponent(componentName);
//        startActivity(intent);

//        //TODO 开启设置界面
//        Intent intent =  new Intent(Settings.ACTION_SETTINGS);
//        startActivity(intent);


    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"onRestart");
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");
    }

    @Override
    public void onBackPressed() {
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

}
